/// <reference path="jquery.js" />
/// <reference path="select2.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery-ui-1.10.4.js" />
function Drug(name) {
    this.name = name;
    this.timesTaken = 0;
}

Drug.prototype.increment = function() {
    ++this.timesTaken;
};

Drug.prototype.toString = function() {
    return this.name + " has been taken " + this.timesTaken + " times";
};
var InjectionsWidget = function(id, model) {
    this.id = id;
    this.drugsModel = model;
    this.currDrug = model[0];

    return this;
};
InjectionsWidget.prototype.initialize = function() {
    var modelSize = this.drugsModel.length;
    for (var ctr = 0; ctr < modelSize; ++ctr) {
        $("#ddlDrugs" + this.id).append('<option value=' + ctr + '>' + this.drugsModel[ctr].name + '</option>');
    }
    $("#ddlDrugs" + this.id).select2();

    $("#btnInject" + this.id).button({ label: "Inject" });

    this.HookUpEventHandlers();
};
InjectionsWidget.prototype.HookUpEventHandlers = function() {
    console.log("Hooking up button handler on to btnInject" + this.id);
    var caller = this;
    $("#btnInject" + this.id).click(function() {
        caller._HandleClickEvent(caller);
    });

    $("#ddlDrugs" + this.id).change(function() {
        caller._HandleChangeEvent(caller);
    });
};
InjectionsWidget.prototype._HandleClickEvent = function(caller) {
    console.log("Logging on click: " + caller.currDrug);
    caller.currDrug.increment();
    $("#lblInjectionsCount" + caller.id).text(caller.currDrug.timesTaken);
};
InjectionsWidget.prototype._HandleChangeEvent = function(caller) {
    console.log("Logging on change: " + caller.currDrug);
    var newIndex = $("#ddlDrugs" + caller.id).val();
    caller.currDrug = caller.drugsModel[newIndex];
    $("#lblInjectionsCount" + caller.id).text(caller.currDrug.timesTaken);
};
InjectionsWidget.prototype.Render = function() {
    $("#" + this.id).addClass("injections-widget");
    var renderedHtmlMarkup = '<select id="ddlDrugs' + this.id + '" class="ddlDrugs"></select><label id="lblInjectionsCount' + this.id +
        '" class="lblInjectionsCount">' + this.currDrug.timesTaken +
        '</label><hr/><button id="btnInject' + this.id + '"class="btnInject"></button>';

    $("#" + this.id).append(renderedHtmlMarkup);

    this.initialize();
    return renderedHtmlMarkup;
};
$(document).ready(function() {
    var batmanWidget = new InjectionsWidget("batmanWidget", [new Drug("Aspirin"), new Drug("Caffetine Menstrual")]);
    var supermanWidget = new InjectionsWidget("supermanWidget", [new Drug("Prednisone"), new Drug("Clenbuterol")]);
    var greenArrowWidget = new InjectionsWidget("greenArrowWidget", [new Drug("Levofloxacin"), new Drug("Azithromycin")]);

    batmanWidget.Render();
    supermanWidget.Render();
    greenArrowWidget.Render();
});